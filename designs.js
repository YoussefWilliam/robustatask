// Select color input
// Select size input



function makeGrid() {

  
  var h, w, tableElements, rowElements, columnsElements;

  h = document.getElementById('inputHeight').value;
  w = document.getElementById('inputWidth').value;
  color = document.getElementById('colorPicker').value;

  if (h == "" || w == "") {
      alert("Please enter some numeric value");
  } else {
      tableElements = document.createElement('table');

      for (var i = 0; i < h; i++) {
        rowElements = document.createElement('tr');

          for (var j = 0; j < w; j++) {
            columnsElements = document.createElement('td');
              columnsElements.style.borderColor  = color;
              rowElements.appendChild(columnsElements);
          }
          tableElements.appendChild(rowElements);
      }
      document.body.appendChild(tableElements);
  }
}

function clearGrid(){
  location.reload();
  
}
